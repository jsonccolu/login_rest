from django.contrib import admin
from Apps.users.models import User, GeoLocation

# Register your models here.

admin.site.register(User)
admin.site.register(GeoLocation)