from rest_framework import (status, 
                            viewsets, 
                            parsers, 
                            decorators, 
                            generics, 
                            permissions)
                            
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from datetime import datetime
from django.http import Http404
from Apps.users.api.renderers import UserRenderer
from Apps.users.api.serializers import (RegisterSerializer,
                                        UserTokenSerializer, 
                                        UserSerializer, 
                                        UserImageSerializer, 
                                        UserListSerializer,
                                        LoginSerializer,
                                        LogoutSerializer)
from Apps.users.models import User
from django.contrib.auth.models import Group

class UserRegister(viewsets.ModelViewSet):
    serializer_class = RegisterSerializer
    http_method_names = ['post','options','put']
    queryset = User.objects.all()
    renderer_class = (UserRenderer,)
    @decorators.action(
        detail=True,
        methods=['PUT'],
        #serializer_class=UserImageSerializer,
        parser_classes=[parsers.MultiPartParser],
    )
    def upload_image(self, request, pk):
        obj = self.get_object()
        serializer = self.serializer_class(obj,
                                        data=request.data,
                                        partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,
                            status=status.HTTP_201_CREATED)
        return Response(serializer.errors,
                            status.HTTP_400_BAD_REQUEST)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
    
    def perform_create(self, serializer):
        serializer.save()
        
class UserListView(APIView):
    def get(self, request, *arg,**kwargs):
        users = User.objects.all()
        users_serializer = UserListSerializer(users,many=True)
        return Response(users_serializer.data,status=status.HTTP_200_OK)

class UserDetailView(generics.GenericAPIView):

    serializer_class = UserListSerializer
    def get_object(self, username=None):
        try:
            return User.objects.get(username=username)
        except User.DoesNotExist:
            raise Http404

    def get(self,request, username=None):
        user = self.get_object(username)
        user_serializer = UserListSerializer(user)
        return Response(user_serializer.data,status=status.HTTP_200_OK)

    def put(self,request,username=None):
        user = self.get_object(username)
        user_serializer = UserListSerializer(user)
        if user_serializer.is_valid():
            user_serializer.save()
            return Response(user_serializer.data, status=status.HTTP_200_OK)
        return Response(user_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def delete(self,request,username=None):
        user = self.get_object(username)
        user.delete()
        return Response({'message': 'Usuario eliminado correctamente'})

class Login(generics.GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status= status.HTTP_200_OK)

class Logout(generics.GenericAPIView):
    serializer_class = LogoutSerializer

    #permission_classes = (IsAuthenticated,)

    def post(self, request):
        serializer = self.serializer_class(data = request.data)

        serializer.is_valid(raise_exception=True)
        serializer.save()

        session_message = 'Sesiones de usuario Eliminada.'
        token_message = 'Token eliminado.'

        return Response({
                'token_message':token_message,
                'session_message':session_message,
            }, status=status.HTTP_204_NO_CONTENT)