from django.db import models
from django.contrib.auth.models import (BaseUserManager, 
                                        AbstractBaseUser, 
                                        PermissionsMixin)
from django.contrib.auth.models import Group
from phonenumber_field.modelfields import PhoneNumberField

from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib import auth
from rest_framework.exceptions import AuthenticationFailed
from rest_framework_simplejwt.tokens import RefreshToken

def logo_dir_path(instance, filename):
    extension = filename.split('.')[-1]
    og_filename = filename.split('.')[0]
    new_filename = "perfil/%s_%s.%s" % (instance.name,instance.username, extension)
    return new_filename

class GeoLocation(models.Model):
    latitude = models.DecimalField(max_digits=13, decimal_places=6)
    longitude = models.DecimalField(max_digits=13, decimal_places=6)
    #street = models.CharField(max_length=400,blank=True)
    #city = models.CharField(max_length=255, blank=True)
    #state = models.CharField(max_length=255,null=True, blank=True)

    def __str__(self):
        return f'{self.latitude} {self.longitude}'

class UserManager(BaseUserManager):
    def _create_user(self, username, email, name, last_name, password, is_staff, is_superuser, **extra_fields):
        """Create neww user"""
        user = self.model(
            username=username,
            email=email,
            name=name,
            last_name=last_name,
            is_staff=is_staff,
            is_superuser=is_superuser,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self.db)
        return user

    def create_user(self, username, email, name, last_name, password=None, **extra_fields):
        return self._create_user(username, email, name, last_name, password, False, False, **extra_fields)
    
    def create_superuser(self, username, email, name, last_name, password=None, **extra_fields):
        return self._create_user(username, email, name, last_name, password, True, True, **extra_fields)

class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField('Nombre de Usuario',max_length=255, unique=True, db_index=True)
    email = models.EmailField('Dirección de Correo Electronico',max_length=255, unique=True, db_index=True)
    name = models.CharField('Nombres' ,max_length=255, blank=True, null=True)
    last_name = models.CharField('Apellidos',max_length=255, blank=True, null=True)
    image = models.ImageField('Imagen de Perfil',upload_to=logo_dir_path,max_length=255,null=True, blank=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    groups = models.ManyToManyField(Group, blank=True, related_name="user_set")

    bussines_name = models.CharField('Razon Social',max_length=255, blank=True, null=True)
    ruc = models.IntegerField('RUC',default=0)
    location = models.OneToOneField(GeoLocation,on_delete=models.CASCADE, blank=True, null=True)
    phone_number = PhoneNumberField() 

    objects = UserManager()

    class Meta:
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'
    
    USERNAME_FIELD = 'email' #'username'
    REQUIRED_FIELDS = ['username','name','last_name']

    def tokens(self):
        refresh = RefreshToken.for_user(self)
        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token)
        }

    def __str__(self):
        return f'{self.email}'

        