from rest_framework import serializers
from rest_framework.serializers import ValidationError

from Apps.users.models import User, GeoLocation
from django.contrib.auth.models import Group
from django.contrib.auth.password_validation import validate_password

from phonenumber_field.serializerfields import PhoneNumberField
from phonenumber_field.phonenumber import to_python

from django.contrib import auth
from rest_framework_simplejwt.tokens import RefreshToken, TokenError

class GeolocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = GeoLocation
        fields = '__all__'

class UserImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('image')

class CustomPhoneNumberField(PhoneNumberField):
    def to_internal_value(self, data):
        phone_number = to_python(data)
        if phone_number and not phone_number.is_valid():
            raise ValidationError(self.error_messages["invalid"])
        return phone_number.as_e164

class RegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=68, min_length=8,write_only=True)
    image = serializers.ImageField(read_only=True)
    location = GeolocationSerializer()
    phone_number = CustomPhoneNumberField()

    class Meta:
        model = User
        fields = ('name', 'last_name', 'email', 'username',
                    'password', 'image', 'bussines_name', 
                    'ruc', 'phone_number', 'location', 'groups')

    def validate_password(self, value):
        validate_password(value)
        return value
    
    def validate(self, attrs):
        email = attrs.get('email', '')
        username = attrs.get('username', '')

        if not username.isalnum():
            raise serializers.ValidationError(
                {'username': 'The username should only contain alphanumeric characters'}
            )
        return attrs

    def create(self,validated_data):
        location = validated_data.pop('location')
        groups = validated_data.pop('groups')
        user = User(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        for group in groups:
            user.groups.add(group)
        
        loc_serializer = GeolocationSerializer(data=location)

        if loc_serializer.is_valid():
            loc = loc_serializer.save()
            user.location=loc

        return user

class UserSerializer(serializers.ModelSerializer):
    image = serializers.ImageField(read_only=True)
    location = GeolocationSerializer()
    phone_number = CustomPhoneNumberField()
    class Meta:
        model = User
        fields = ('name', 'last_name', 'email', 'username',
                   'password', 'image', 'bussines_name', 
                   'ruc', 'phone_number', 'location', 'groups')

    def validate_password(self, value):
        validate_password(value)
        return value

    def create(self,validated_data):
        location = validated_data.pop('location')
        groups = validated_data.pop('groups')
        user = User(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        for group in groups:
            user.groups.add(group)
        
        loc_serializer = GeolocationSerializer(data=location)

        if loc_serializer.is_valid():
            loc = loc_serializer.save()
            user.location=loc

        return user
    
    """def update(self,instance,validated_data):
        update_data = super().update(instance,validated_data)
        update_data.set_password(validated_data['password'])
        update_data.save()
        return update_data"""

class LoginSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()
    password = serializers.CharField(max_length=68, min_length=8, write_only=True)
    username = serializers.CharField(max_length=68, min_length=8, read_only=True)

    tokens = serializers.SerializerMethodField()
    class Meta:
        model = User
        fields = ['email', 'password', 'username', 'tokens']

    def get_tokens(self,obj):
        user = User.objects.get(email=obj['email'])

        return {
            'refresh': user.tokens()['refresh'],
            'access': user.tokens()['access']
        }

    def validate(self, validated_data):
        email = validated_data['email']
        password = validated_data['password']
        
        filter_user = User.objects.filter(email=email)

        user = auth.authenticate(email=email, password=password)

        if not user:
            raise AuthenticationFailed('Invalid credentials, try again')
        
        if not user.is_active:
            raise AuthenticationFailed('Account disabled, contact admin')

        return {
            'email':user.email,
            'username': user.username,
            'token': user.tokens,
        }

        return super().validate(validated_data)

class LogoutSerializer(serializers.Serializer):
    refresh = serializers.CharField()

    default_error_message = {
        'bad_token': ('Token is expired or invalid')
    }

    def validate(self, validated_data):
        self.token = validated_data['refresh']

        return validated_data
    
    def save(self, **kwargs):

        try:
            RefreshToken(self.token).blacklist()
        
        except TokenError:
            self.fail(self.default_error_messages)

class UserListSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username','name','last_name','email','image','groups')

    """def to_representation(self,instance):
        return {
            'username': instance['username'],
            'name': instance['name'],
            'last_name': instance['last_name'],
            'email' : instance['email'],
            'groups' : instance['groups']
        }"""

class UserTokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username','email','name','last_name')

