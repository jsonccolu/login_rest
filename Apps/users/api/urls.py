from django.urls import path, include
from Apps.users.api.api import user_api_view,user_detail_api_view
from Apps.users.views import UserRegister, UserListView, UserDetailView
from rest_framework_simplejwt.views import TokenRefreshView
from Apps.users.views import Login, Logout

from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'', UserRegister)

urlpatterns = [
    #path('register/',UserRegister.as_view(), name = 'registrar usuario'),
    path('users/register/',include(router.urls)),
    path('users/list/',UserListView.as_view(), name = 'listar usuarios'),
    path('users/detail/<slug:username>/',UserDetailView.as_view(),name = 'info usuario'),
    path('auth/login/',Login.as_view(),name='Login'),
    path('auth/logout/', Logout.as_view(),name = 'Logout'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]