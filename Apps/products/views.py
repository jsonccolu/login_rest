from django.shortcuts import render
#from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from Apps.products.models import Products
from Apps.products.serializers import ProductSerializer
from Apps.products.permissions import IsOwner
from rest_framework_simplejwt import authentication
from rest_framework import permissions, viewsets

class ProductListAPIView(ListCreateAPIView):
    serializer_class = ProductSerializer
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Products.objects.all()
    def perform_create(self, serializer):
        return serializer.save(owner=self.request.user)

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user)

class ProductDetailAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = ProductSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner,)
    queryset = Products.objects.all()
    lookup_field = "id"

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user)