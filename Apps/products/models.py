from django.db import models
from Apps.users.models import User

# Create your models here.

class Products(models.Model):
    name = models.CharField('nombre de Producto',max_length=150,unique=True,blank=False,null=False)
    description = models.TextField('Descripción de Producto',max_length=200)
    peso = models.DecimalField('peso', max_digits=13, decimal_places=2,blank=False,null=False)
    offer = models.CharField('oferta',max_length=200,blank=True,null=False)
    price = models.DecimalField('precio',max_digits=13, decimal_places=2, blank=False,null=False)
    stock_init = models.PositiveIntegerField('stock inicial',blank=False, null=False)
    owner = models.ForeignKey(to=User, on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)