from rest_framework import serializers
from Apps.products.models import Products

class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Products
        exclude = ('created_at', 'updated_at',)

    def to_representation(self,instance):
        return {
            'id': instance.id,
            'name': instance.name,
            'description': instance.description,
            'peso': instance.peso,
            'offer': instance.offer,
            'price': instance.price,
            'stock': instance.stock,
        }