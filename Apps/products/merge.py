import numpy

def merge_sort(A):
    if len(A)<=1:
        return A
    
    middle = len(A) // 2
    left = A[:middle]
    right = A[middle:]

    left = merge_sort(left)
    right = merge_sort(right)

    return merge(left, right)

def merge(left, right):
    Result = []

    l_i, r_i = 0, 0

    while l_i< len(left) and r_i < len(right):
        if left[l_i] <= right[r_i]:
            Result.append(left[l_i])
            l_i += 1

        else:
            Result.append(right[r_i])
            r_i += 1
    
    if l_i < len(left):
        Result.extend(left[l_i:])
    
    if r_i < len(right):
        Result.extend(right[r_i:])
    
    return Result

A =list( [2,3,4,5,1,2])

print(merge_sort(A))