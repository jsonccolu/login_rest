from django.urls import path, include
from Apps.products.views import ProductDetailAPIView, ProductListAPIView
from rest_framework.routers import DefaultRouter

urlpatterns = [
    path('', ProductListAPIView.as_view(), name="products"),
    path('<int:id>/', ProductDetailAPIView.as_view(), name="products"),
]